require 'rails_helper'

RSpec.describe Spree::Product, type: :decorator do
  describe 'related_product' do
    let(:taxon1)   { create(:taxon, name: "ruby") }
    let(:taxon2)   { create(:taxon, name: "rails") }
    let(:taxon3)   { create(:taxon, name: "on") }
    let(:product1) { create(:product, taxons: [taxon1]) }
    let(:product2) { create(:product, taxons: [taxon2]) }
    let(:product3) { create(:product, taxons: [taxon1, taxon2]) }
    let(:product4) { create(:product, taxons: [taxon1, taxon3]) }

    it 'matches products with shared taxon' do
      expect(Spree::Product.related_product(product1)).to match_array([product3, product4])
    end

    it 'not include products with shared taxon' do
      expect(Spree::Product.related_product(product1)).not_to include product2
    end

    it 'not include self_product with shared taxon' do
      expect(Spree::Product.related_product(product1)).not_to include product1
    end
  end
end
