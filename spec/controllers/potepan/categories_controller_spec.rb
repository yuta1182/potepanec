require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show controller @taxon' do
    let(:taxon) { create(:taxon) }
    let(:taxonomies) { create(:taxonomy) }
    let(:product1) { create(:product, taxons: [taxon]) }
    let(:product2) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'successful show page' do
      expect(response).to be_successful
    end

    it 'http status' do
      expect(response.status).to eq(200)
    end

    it 'render_template :show taxon.id' do
      expect(response).to render_template(:show)
    end

    it 'assigns the requested taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    context '@taxonomies controller test' do
      it 'assigns the requested taxonomies' do
        expect(assigns(:taxonomies)).to include taxonomies
      end
    end

    context '@products controller test' do
      it 'assigns the requested prodoct1' do
        expect(assigns(:products)).to match_array product1
      end

      it 'not assigns the requested product2' do
        expect(assigns(:products)).not_to include product2
      end
    end
  end
end
