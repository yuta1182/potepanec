require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show controller' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'successful show page' do
      expect(response).to be_successful
    end

    it 'http status' do
      expect(response.status).to eq(200)
    end

    it 'render_template :show product.id' do
      expect(response).to render_template(:show)
    end

    it 'assigns the requested product' do
      expect(assigns(:product)).to eq product
    end

    it 'assigns the requested related_product size eq limit size' do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
