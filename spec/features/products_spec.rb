require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon)            { create(:taxon, name: "taxon") }
  let!(:product)          { create(:product, name: "products", taxons: [taxon]) }
  let!(:related_product)  { create(:product, taxons: [taxon], name: "ruby", price: 20) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "visit product page" do
    expect(page).to have_title product.name
  end

  scenario "_light_section correct working" do
    expect(page).to have_title product.name
    within '.page-title' do
      expect(page).to have_content product.name
    end
    within '.breadcrumb' do
      expect(page).to have_content product.name
      click_link 'Home'
    end
    expect(current_path).to eq potepan_index_path
  end

  scenario "_main_content_section correct working" do
    within all('.media-body')[2] do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    expect(page).to have_link '一覧ページへ戻る'
    click_link '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(related_product.taxons.first.id)
  end

  scenario "_related_products correct working" do
    within '.productCaption' do
      expect(page).to have_content related_product.name
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
      visit potepan_product_path(product.id)
      expect(page).to have_content related_product.price
      click_link related_product.price
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end
end
