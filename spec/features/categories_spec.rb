require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxons) { create(:taxon, name: "taxons") }
  let!(:taxonomies) { create(:taxonomy, name: "Category") }
  let!(:child) { taxonomies.root.children.create(name: "child") }
  let!(:mug) { child.children.create(name: "mug") }
  let!(:clothing) { child.children.create(name: "clothing") }
  let!(:bag) { clothing.root.children.create(name: "bag") }
  let!(:product) { create(:product, name: "product", price: 19.99, taxons: [mug]) }

  scenario "visit category page" do
    visit potepan_category_path(child.id)
    expect(page).to have_title child.name
  end

  scenario "_category correct working" do
    visit potepan_category_path(child.id)
    within '.side-nav' do
      expect(page).to have_content taxonomies.name
      expect(page).to have_content mug.name
      click_link mug.name
      visit potepan_category_path(mug.id)
      expect(page).to have_content clothing.name
      click_link clothing.name
      visit potepan_category_path(clothing.id)
      expect(page).to have_content bag.name
      click_link bag.name
      visit potepan_category_path(bag.id)
    end
  end

  scenario "_products correct working" do
    visit potepan_category_path(mug.id)
    within('.productBox') do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      click_link product.name, href: potepan_product_path(product.id)
    end
  end
end
