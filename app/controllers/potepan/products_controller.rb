class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_NUMBER = 4

  def show
    @product = Spree::Product.includes(master: [:images, :default_price]).find(params[:id])
    @related_products = Spree::Product.related_product(@product).limit(MAX_DISPLAY_NUMBER)
  end
end
